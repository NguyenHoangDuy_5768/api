﻿// Please see documentation at https://learn.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
// Sử dụng Fetch API hoặc Axios 
fetch('https://your-api-url/api/products')
    .then(response => response.json())
    .then(products => {
        // Xử lý danh sách sản phẩm 
        console.log(products);
    })
    .catch(error => console.error('Error:', error)); 
// Thay {id} bằng ID cụ thể của sản phẩm 
const productId = 1;
fetch(`https://your-api-url/api/products/${productId}`)
    .then(response => response.json())
    .then(product => {
        // Xử lý thông tin chi tiết sản phẩm 
        console.log(product);
    })
    .catch(error => console.error('Error:', error)); 
// Thông tin sản phẩm mới cần tạo 
const newProduct = {
    name: 'New Product',
    price: 100,
    description: 'A new product',
    // Thêm các thông tin khác 
};
fetch('https://your-api-url/api/products', {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
    },
    body: JSON.stringify(newProduct),
})
.then(response => response.json()) 
.then(createdProduct => { 
// Xử lý thông tin sản phẩm đã tạo 
console.log(createdProduct); 
}) 
    .catch(error => console.error('Error:', error)); 
// Thay {id} và cập nhật thông tin sản phẩm 
const productIdToUpdate = 1;
const updatedProduct = {
    id: productIdToUpdate,
    name: 'Updated Product',
    price: 150,
    description: 'An updated product',
    // Thêm các thông tin khác 
}; 
fetch(`https://your-api-url/api/products/${productIdToUpdate}`, {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json',
    },
    body: JSON.stringify(updatedProduct),
})
    .then(response => {
        if (response.status === 204) {
            console.log('Product updated successfully.');
        } else {
            console.error('Failed to update product.');
        }
    })
    .catch(error => console.error('Error:', error));
// Thay {id} bằng ID cụ thể của sản phẩm cần xóa 
const productIdToDelete = 1;
fetch(`https://your-api-url/api/products/${productIdToDelete}`, {
    method: 'DELETE',
})
    .then(response => {
        if (response.status === 204) {
            console.log('Product deleted successfully.');
        } else {
            console.error('Failed to delete product.');
        }
    })
    .catch(error => console.error('Error:', error));
